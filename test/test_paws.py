from mock import patch
from unittest import TestCase
import paws


class S3TestCase(TestCase):
    def setUp(self):
        super(S3TestCase, self).setUp()
        self.boto_mock = patch.object(paws, 'boto3', autospec=True).start()
        self.client_mock = self.boto_mock.client.return_value

    def test_create_bucket_simple(self):
        paws.create_s3_bucket(name="Test Bucket")
        self.client_mock.create_bucket.assert_called_with(
            Bucket='Test Bucket',
            ACL='private',
            CreateBucketConfiguration={'LocationConstraint': 'us-east-1'})
