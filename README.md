pAWS
---
[![Run Status](https://api.shippable.com/projects/577fd6683be4f4faa56c3a67/badge?branch=master)](https://app.shippable.com/projects/577fd6683be4f4faa56c3a67)
[![Coverage Badge](https://api.shippable.com/projects/577fd6683be4f4faa56c3a67/coverageBadge?branch=master)](https://app.shippable.com/projects/577fd6683be4f4faa56c3a67)

Custom python AWS resource uploader and viewer.
