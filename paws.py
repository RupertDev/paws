import boto3
import json
import os


def get_s3_buckets():
    client = boto3.client('s3')
    return client.list_buckets()


def list_s3_buckets():
    resp = get_s3_buckets()
    for bucket in resp['Buckets']:
        print bucket['Name']


def create_s3_bucket(name, ACL='private', region=None):
    client = boto3.client('s3')
    try:
        resp = client.create_bucket(
            Bucket=name,
            ACL=ACL,
            CreateBucketConfiguration={
                'LocationConstraint': region if region else 'us-east-1'})
    except Exception as e:
        print "Something went wrong: " + e


def delete_s3_bucket(name):
    client = boto3.client('s3')
    try:
        resp = client.delete_bucket(Bucket=name)
    except Exception as e:
        print "Something went wrong: " + e


def upload_file(file_path, bucket, object_name, destination='s3'):
    if os.path.isfile(file_path):
        if destination == 's3':
            s3 = boto3.resource('s3')
            s3.meta.client.upload_file(file_path, bucket, object_name)
        else:
            print "Destination:", destination, "not yet implemented."
    else:
        print 'File: {} doesn\'t exist.'.format(file_name)
